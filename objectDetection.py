''' Importing libraries '''
import os
import sys

sys.path.append(os.getcwd()+"/model/")
sys.path.append(os.getcwd()+"/model/Utils/")
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import argparse
from keras_yolo3.yolo import YOLO, detect_video
from PIL import Image
from timeit import default_timer as timer
from utils import load_extractor_model, load_features, parse_input, detect_object
import test
import utils
import pandas as pd
import numpy as np
from Get_File_Paths import GetFileList
import random

from matplotlib import pyplot
from matplotlib.patches import Rectangle
import cv2
import base64
import json
from io import BytesIO

model_weights = os.getcwd()+"/model/trained_weights_final.h5"
model_classes = os.getcwd()+"/model/data_classes.txt"
anchors_path = os.getcwd()+"/model/keras_yolo3/model_data/yolo_anchors.txt"
score = 0.25
FLAGS = None
gpu_num = 1
detection_results_file = os.getcwd()+"/Detection_Results.csv"

# get all of the results above a threshold

def paste(background,foreground,pos=(0,0)):
    #get position and crop pasting area if needed
    x = pos[0]
    y = pos[1]
    bgWidth = background.shape[0]
    bgHeight = background.shape[1]
    frWidth = foreground.shape[0]
    frHeight = foreground.shape[1]
    width = bgWidth-x
    height = bgHeight-y
    if frWidth<width:
        width = frWidth
    if frHeight<height:
        height = frHeight
    # normalize alpha channels from 0-255 to 0-1
    alpha_background = background[x:x+width,y:y+height,3] / 255.0
    alpha_foreground = foreground[:width,:height,3] / 255.0
    # set adjusted colors
    for color in range(0, 3):
        fr = alpha_foreground * foreground[:width,:height,color]
        bg = alpha_background * background[x:x+width,y:y+height,color] * (1 - alpha_foreground)
        background[x:x+width,y:y+height,color] = fr+bg
    # set adjusted alpha and denormalize back to 0-255
    background[x:x+width,y:y+height,3] = (1 - (1 - alpha_foreground) * (1 - alpha_background)) * 255
    return background

def image_base64(im):
    if isinstance(im, str):
        im = get_thumbnail(im)
    with BytesIO() as buffer:
        im.save(buffer, 'jpeg')
        return base64.b64encode(buffer.getvalue()).decode()
'''
 Returns:
      prediction: list of bounding boxes in format (xmin,ymin,xmax,ymax,class_id,confidence)
      image: unaltered input image as (H,W,C) array
'''
def coreDetection(imageData, threshhold_pixels=3):
    # define YOLO detector
    yolo = YOLO(
        **{
            "model_path": model_weights,
            "anchors_path": anchors_path,
            "classes_path": model_classes,
            "score": score,
            "gpu_num": gpu_num,
            "model_image_size": (416, 416),
        }
    )

    # labels to draw on images
    class_file = open(model_classes, "r")
    input_labels = [line.rstrip("\n") for line in class_file.readlines()]
    print("Found {} input labels: {} ...".format(len(input_labels), input_labels))
    im = Image.open(imageData)
    prediction, image = detect_object(
                yolo,
                imageData,
                save_img=False,
                save_img_path=os.getcwd()+"/",
                postfix="_result",
            )
    print(prediction)
    json_data = {}
    BBox = []
    rawImage = cv2.imread(imageData)
    thickness = 1
    for data in prediction:
        print(data)
        xmin, ymin, xmax, ymax = data[:4]        
        x, y = xmin, ymin
        w, h = xmax - xmin, ymax - ymin
        label = input_labels[data[4]] 
        im_crop = im.crop((xmin+threshhold_pixels, ymin+threshhold_pixels, xmax+threshhold_pixels, ymax+threshhold_pixels))
        # white blank image
        blank_image = Image.new('RGB', (512,512), (255, 255, 255))
        # resize the image
        size = (512,512)
        background = blank_image.resize(size,Image.ANTIALIAS)
        background.paste(im_crop, (50, 50))
        myimage = image_base64(background)
        # Image._show(background)
        assemble_data = { "top": int(y), "left": int(x), "width": int(w), "height": int(h), "label": label, "image_base64": myimage}
        BBox.append(assemble_data)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
    json_data = {"BoundingBox": BBox}
    return json_data
