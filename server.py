import os
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import requests
import objectDetection
import json
import datefinder
from base64 import decodestring, b64decode
import random
import string
import re

app = Flask(__name__)
api = Api(app)
GvisionAPI_key = "AIzaSyCj7YXJyFR3Ep1I0uLBzf6qeB0L9KEFcI4"
url = "https://vision.googleapis.com/v1/images:annotate?key="+GvisionAPI_key
headers = {
  'Content-Type': 'application/json'
}
# {
#     "flag": "Trainer",
#     "ocr_word": "RED",

#     "modified_word": ["RED Color"]
# }

@app.route('/getVersion', methods=["post"])
def ping():
    return "Success"

@app.route('/train',methods=['post'])
def Train():
             
    if request.method == "POST":
        inputWord =  request.get_json()
        originalWord = inputWord["original_word"]
        modifiedWord = inputWord["modified_word"]
        print(originalWord,modifiedWord)
        with open('json_dataset.json') as json_file:
            data = json.load(json_file)
            print(data) 
            # jsonArray = (str(data).lower().split('\n'))
            # print(jsonArray)

        try:
         for trainText in  data['train_texts']:
            if (originalWord == trainText['original_word']):
             print (trainText['original_word'])
             for modWord in trainText['modified_word']:
                jsonModifiedWord = modWord
                if(modifiedWord == jsonModifiedWord ):
                    print(jsonModifiedWord)
                else:
                    trainText['modified_word'].append(modifiedWord)
                     
            else:
                data['train_texts'].append(
                    {
                        "original_word" : originalWord,
                        # "modified_word" : modified_word
                    }
                )
                with open('json_dataset.json', 'w') as outFile:
                    json.dump(data, outFile)
                print(data)
                pass
        except Exception as e:
         pass
        return "success"

@app.route('/image', methods=['post'])
def ocrImg():
    if request.method == "POST":
        base64String =  request.get_json()
        # print(base64String)
        fileName = os.getcwd()+"/tempImage/"+get_random_string(16)+".png"
        # fileName = os.getcwd()+"/tempImage/fwhlumitdnxyoskq.png"
        image_string = base64String["image"]
        with open(fileName,"wb") as f:
            f.write( b64decode(base64String['image']))
        logo_text = ""
        title_text = ""
        address = ""
        mobile_no = ""
        content_items = ""
        parse_content_items = []
        parse_items_len = ""
        product_name = []
        product_quantity = []
        product_price = []
        product_list = []
        tax = ""
        total = ""
        date = []
        raw_text = ""
        salesTax = ""
        totalIndex = ""
        rawFullText = ""
        payload = "{\n  \"requests\":[\n    {\n      \"image\":{\n        \"content\":\""+image_string+" \"\n      },\n      \"features\":[\n        {\n          \"type\":\"DOCUMENT_TEXT_DETECTION\",\n          \"maxResults\":1\n        }\n      ]\n    }\n  ]\n}\n"
        response = requests.request("POST", url, headers=headers, data = payload)
        raw_text = response.json()['responses'][0]['fullTextAnnotation']['text']
        rawFullText = raw_text
        raw_text_arr = []
        raw_text_arr = (str(raw_text).lower().split('\n'))
        print(raw_text_arr)
       
    try:
        totalValue = ""
        totalcfgValue = ""

        with open(os.getcwd()+'/cfg/Total.json') as f:
            totalcfgValue = json.load(f)
            print(totalcfgValue['name'])
        try:
            for name in totalcfgValue['name']:
                try:
                    totalIndex = raw_text_arr.index(name)  
                    totalValue = totalIndex + 1    
                except Exception as ae:
                    pass
        except Exception as ae:
            print(ae)
            pass

        print(totalValue)
        print("*************************")
        # subtotalIndex = raw_text_arr.index('sub-total')
        
        # print ("subtotalIndex = ")
        # print (subtotalIndex)
        # isSubtotal1 = raw_text_arr[subtotalIndex + 1]
        # isSubtotal2 = raw_text_arr[subtotalIndex + 2]
        # subTotalValue = isSubtotal1 if isSubtotal1.isnumeric() else isSubtotal2
        # print("subTotalValue = ")
        # print(subTotalValue)
        # salesTax = ""
        # salesTax = raw_text_arr[subtotalIndex + 3] if subTotalValue == isSubtotal2 else raw_text_arr[subtotalIndex + 2] 
        # print("Sales Tax =")
        # print(salesTax)    


        for i in re.findall(r'[\+\(]?[1-9][0-9 .\-\(\)]{8,}[0-9]', raw_text_arr):
            mobile_no = i
            print("Mobile No =")
            print(i)
       
        matches = datefinder.find_dates(raw_text_arr)
        print(matches)
        dateArr = []
        for match in matches:
                print(match)
                dateArr.append(match)
        date = dateArr
        print(date)

        pass
    except Exception as e:
        print(e)
        pass
    
        
    threshhold_pixels = base64String["threshhold_pixels"]
    ''' Insert image to object detection algorithm '''
    data = objectDetection.coreDetection(fileName, threshhold_pixels) #Get Meta_data
    imageMetaDataarr = data['BoundingBox']
    testarr= []

    for metaData in imageMetaDataarr:
        label = metaData['label']
        image_base64 = metaData['image_base64']
        op_data = {label: ""}
        payload = "{\n  \"requests\":[\n    {\n      \"image\":{\n        \"content\":\""+image_base64+" \"\n      },\n      \"features\":[\n        {\n          \"type\":\"DOCUMENT_TEXT_DETECTION\",\n          \"maxResults\":1\n        }\n      ]\n    }\n  ]\n}\n"
        response = requests.request("POST", url, headers=headers, data = payload)
        
        try:
            raw_text = response.json()['responses'][0]['fullTextAnnotation']['text']
            raw_text = str(raw_text).replace('\n','')
            # print(raw_text)
            op_data[label] = raw_text
            print(label+"::"+raw_text)
            pass
        except Exception as e:
            pass
        
        if label == "Mobile_no":
            mobile_no = raw_text

        if label == "amount_details":
            total = raw_text
        
        if label == "tax":
            tax == raw_text
        
        if label == "Shop_Name":
            title_text = raw_text
        
        if label == "Shop_Address":
            if address != "":
                address +=", "+raw_text
            raw_text = address
            print(address)
            address = raw_text
        
        # if label == "Date":
        #     if len(date) != 0:
        #         dates +=", "+raw_text
        #         raw_text = dates
        #         print(dates)
        #     matches = datefinder.find_dates(raw_text)
        #     print(matches)
        #     dateArr = []
        #     for match in matches:
        #         print(match)
        #         dateArr.append(match)
        #     date = dateArr
        
        if label == "Content_Items":
            if content_items != "":
                content_items +=", "+raw_text
                raw_text = content_items
                print(content_items)
            content_items = raw_text
      
        
        if label == "Shop_Logo":
            if logo_text != "":
                logo_text +=", "+raw_text
            logo_text = raw_text
        raw_text = ""
        testarr.append(op_data)

        if len(date)==0:
            matches = datefinder.find_dates(rawFullText)
            print(matches)
            dateArr = []
            for match in matches:
                print(match)
                dateArr.append(match)
            date = dateArr
        
        if mobile_no == "":
            for i in re.findall(r'[\+\(]?[1-9][0-9 .\-\(\)]{8,}[0-9]', rawFullText):
                mobile_no = i
                break

   
    parse_content_items = content_items.split(' ') 
    print("parse_content_items===")
    
    print(parse_content_items)
    # tempindex = ""
    for i,indexVal in enumerate(range(len(parse_content_items))):
        if parse_content_items[i].isalpha():
            print(parse_content_items[i])
            product_name_index=parse_content_items.index(parse_content_items[i])
            lastIndex = parse_content_items.index(parse_content_items[-1])
            
            if product_name_index != lastIndex:
                if parse_content_items[product_name_index+1].isalpha():
                 product_name.append(parse_content_items[i]+''+parse_content_items[i+1])
            else:
                # if parse_content_items[product_name_index].isalpha():
                    product_name.append(parse_content_items[i])
            
            print('\n')

        elif parse_content_items[i].isdigit():
            product_quantity.append(parse_content_items[i])
    
    product_price = [i for i in parse_content_items if i[0] == '$']
    # product_price = product_price.split('$')
    
        
        # elif parse_content_items[i].startswith('$') | parse_content_items[i].startswith(''):
        #     product_price.append(parse_content_items[i])

        # product_price = filter(lambda x: x.startswith('$'), parse_content_items)
       
    # for i in parse_content_items:
    #         print('parse=')
    #         print(i)
    #         # product_list = product_list.append([i for i in content_items.split(' ') if not i.isdigit()])
    #         # product_list = ''.join([i for i in parse_content_items if i.islower])
    #         if parse_content_items.islower:
    #           product_name.append(','+ parse_content_items) 
    #         print("product_name")
    #         print(product_name)
    #         break
    # product_price = parse_content_items[-1]

        

    # result = {"Shop_Logo": logo_text, "Shop_Name": title_text, "Shop_Address": address, "Date": date, "Mobile_No": mobile_no, "Content_Items": content_items,"Product_Name": product_name,"Product_Price": product_price,"Product_quantity":product_quantity, "tax": tax if tax != "" else salesTax , "Total": total if total!="" else totalIndex, "rawText": rawFullText}
    result = {"Shop_Logo": logo_text if logo_text != "" else title_text, "Shop_Name": title_text, "Date": date, "Mobile_No": mobile_no, "Product_Name": product_name,"Product_Price": product_price, "Total": total if total!="" else totalIndex, "rawText": rawFullText}

    os.remove(fileName)
    return result

#shopname or shoplogo
#date
#product name
#product price
#total
#tax

#headphone 1 $15.68

# productname = [ headphone, mouse, keybaord]
#product_quantity = [1, 1, 2]
#product_price =  [ $15.68, $12, $15]





# get random string without repeating letters
def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.sample(letters, length))
    print("Random String is:", result_str)
    return result_str


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5001', debug=False, threaded=True)
